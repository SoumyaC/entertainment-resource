import { combineReducers } from 'redux';
import appReducer from 'common/reducer';

export default combineReducers({
  app: appReducer,
});
