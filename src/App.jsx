import { hot } from 'react-hot-loader/root';
import { Switch, Route } from 'react-router-dom';
import MoviesDetailsPage from 'movieDetails';
import landingPage from './landingPage';

class App extends React.Component {
  render() {
    return (
      <Switch>
        <Route path={`/movies/:id`} component={MoviesDetailsPage} />
        <Route path={`/`} component={landingPage} />
      </Switch>
    );
  }
}

export default hot(App);
