import { fetchLatestMoviesType, fetchMovieDetailsType } from 'common/actions';
import { getInitState } from 'utils/async-middleware';

const initialState = {
  latest: getInitState(),
  movieDetails: getInitState(),
};

export default (state = { ...initialState }, action) => {
  switch (action.type) {
    case fetchLatestMoviesType:
      return { ...state, latest: { ...state.latest, ...action } };

    case fetchMovieDetailsType:
      return { ...state, movieDetails: { ...state.movieDetails, ...action } };

    default:
      return state;
  }
};
