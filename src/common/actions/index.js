import axios from 'axios';
const API_URL =
  'https://api.themoviedb.org/3/movie/top_rated?api_key=20b4ce7b5784317b307b0ae6c3dfa97d&adult=false';

export const fetchLatestMoviesType = 'FETCH_LATEST_MOVIES';
export const fetchMovieDetailsType = 'FETCH_MOVIE_DETAILS';

export const fetchLatestMovies = () => (dispatch) => {
  return dispatch({
    type: fetchLatestMoviesType,
    payload: axios.get(API_URL),
  });
};

export const fetchMovieDetails = (id) => (dispatch) => {
  const MOVIE_ID_URL = `https://api.themoviedb.org/3/movie/${id}?api_key=20b4ce7b5784317b307b0ae6c3dfa97d&adult=false`;
  return dispatch({
    type: fetchMovieDetailsType,
    payload: axios.get(MOVIE_ID_URL),
  });
};
