import Spinner from 'react-bootstrap/Spinner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Header from './Header.jsx';
import 'bootstrap/dist/css/bootstrap.min.css';
import { fetchLatestMovies } from 'common/actions';
import history from 'historyConfig';
class LandingPage extends React.Component {
  componentDidMount() {
    const { getLatestMovies } = this.props;
    getLatestMovies();
  }
  render() {
    const { latest } = this.props;

    console.log('print latest.payload', latest.payload);

    if (latest.isLoading) {
      return <Spinner animation="border" variant="warning" />;
    }
    if (latest.hasError) {
      return <div>There was an error with the server</div>;
    }
    if (latest.payload) {
      const { results } = latest.payload.data;
      return (
        <div>
          <Header />
          {results &&
            results.slice(0, 5).map(function (movie, i) {
              return (
                <div key={i}>
                  <img
                    src={`https://image.tmdb.org/t/p/w342${movie.poster_path}`}
                    onClick={() => history.push(`/movies/${movie.id}`)}
                  />
                  <div> Title: {movie.title} </div>
                  <div> Vote Average: {movie.vote_average} </div>
                </div>
              );
            })}
        </div>
      );
    }
    return null;
  }
}

const mapState = ({ app: { latest } }) => ({
  latest,
});

const mapDispatch = {
  getLatestMovies: fetchLatestMovies,
};

export default withRouter(connect(mapState, mapDispatch)(LandingPage));
