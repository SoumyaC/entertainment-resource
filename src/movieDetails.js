import history from 'historyConfig';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchMovieDetails } from 'common/actions';
import Spinner from 'react-bootstrap/Spinner';
import Header from './Header.jsx';
import { match } from 'core-js/fn/symbol';

class MoviesDetailsPage extends React.Component {
  constructor(props) {
    super(props);
    const { match } = props;
    this.state = {
      movieID: match.params.id,
    };
  }
  componentDidMount() {
    const { getMovieDetails } = this.props;
    const { movieID } = this.state;
    getMovieDetails(movieID);
  }

  render() {
    const { movieDetails } = this.props;
    console.log('movieDetails: ', movieDetails);

    if (movieDetails.isLoading) {
      return <Spinner animation="border" variant="warning" />;
    }
    if (movieDetails.hasError) {
      return <div>There was an error with the server</div>;
    }
    if (movieDetails.payload) {
      const { data } = movieDetails.payload;
      console.log('data', data);

      return (
        <div>
          <div> Title: {data.title} </div>
          <div> Vote Average: {data.vote_average} </div>
          <img src={`https://image.tmdb.org/t/p/w342${data.poster_path}`} />
          <div>Movie Description: {data.overview}</div>
        </div>
      );
    }
    return null;
  }
}
const mapState = ({ app: { movieDetails } }) => ({
  movieDetails,
});

const mapDispatch = {
  getMovieDetails: fetchMovieDetails,
};

export default withRouter(connect(mapState, mapDispatch)(MoviesDetailsPage));
