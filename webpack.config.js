const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const AutoDllPlugin = require('autodll-webpack-plugin');

const { dependencies } = require('./package.json');

function recursiveIssuer(m) {
  if (m.issuer) {
    return recursiveIssuer(m.issuer);
  }

  if (m.name) {
    return m.name;
  }

  if (m._chunks) {
    if (m._chunks.name) {
      return m._chunks.name;
    }

    if (m._chunks[0]) {
      return m._chunks[0].name;
    }
  }

  return false;
}

function createWebpackConfig() {
  const dev = process.env.NODE_ENV !== 'production';
  const buildDir = path.join(__dirname, 'build');
  const projDir = path.join(__dirname, 'src');
  const commonDir = path.join(projDir, 'common');
  const templatesDir = path.join(__dirname, 'templates');

  const devPlugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ];
  const prodPlugins = [
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
  ];
  const optimizationPlugins = [new HardSourceWebpackPlugin()].concat(
    Object.keys(dependencies).map(
      (vendor) =>
        new AutoDllPlugin({
          filename: `vendor.${vendor}.[${dev ? 'hash' : 'contenthash'}].js`,
          inject: true,
          entry: { [vendor]: vendor },
        })
    )
  );

  return {
    entry: {
      app: path.join(projDir, 'index.js'),
    },

    output: {
      pathinfo: !dev,
      path: buildDir,
      publicPath: '/',
      sourceMapFilename: '[name].map',
      filename: `[name].[${dev ? 'hash' : 'contenthash'}].js`,
      chunkFilename: `[name].[${dev ? 'hash' : 'contenthash'}].js`,
    },

    resolve: {
      cacheWithContext: dev,
      unsafeCache: dev,
      modules: [projDir, 'node_modules'],
      extensions: ['.js', '.jsx', '.css', '.scss', '.module.scss'],
      alias: {
        common: commonDir,
        components: path.join(commonDir, 'components'),
        styles: path.join(commonDir, 'styles'),
        typography: path.join(commonDir, 'typography'),
        utils: path.join(commonDir, 'utils'),
        assets: path.join(projDir, 'assets'),
        data: path.join(projDir, 'data'),
        services: path.join(projDir, 'services'),
        views: path.join(projDir, 'views'),
      },
    },

    cache: dev,

    devtool: dev ? 'cheap-eval-source-map' : false,

    stats: {
      colors: true,
      reasons: true,
      hash: !dev,
      version: !dev,
      timings: true,
      chunks: !dev,
      children: false,
      chunkModules: !dev,
      cached: !dev,
      cachedAssets: !dev,
      errors: true,
      errorDetails: true,
    },

    optimization: {
      namedChunks: true,
      minimizer: [
        new TerserPlugin({
          extractComments: 'all',
          cache: true,
          sourceMap: true,
          chunkFilter: (chunk) => !(chunk.name || '').includes('vendor'),
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
      splitChunks: dev
        ? false
        : {
            chunks: 'all',
            name: true,
            cacheGroups: {
              appStyles: {
                name: 'app',
                test: (m, c, entry = 'app') =>
                  m.constructor.name === 'CssModule' &&
                  recursiveIssuer(m) === entry,
                chunks: 'all',
                enforce: true,
              },
            },
          },
    },

    plugins: optimizationPlugins
      .concat([
        new HtmlWebpackPlugin({
          template: path.join(templatesDir, 'index.template.html'),
          title: 'Entertainment Resource',
          filename: 'index.html',
          favicon: path.join(projDir, 'assets', 'favicon.png'),
          minify: !dev && {
            removeComments: !dev,
            collapseWhitespace: !dev,
            removeRedundantAttributes: !dev,
            useShortDoctype: true,
            removeEmptyAttributes: !dev,
            removeStyleLinkTypeAttributes: !dev,
            keepClosingSlash: true,
            minifyJS: !dev,
            minifyCSS: !dev,
            minifyURLs: !dev,
          },
          baseUrl: dev ? '' : "location.protocol + '//' + location.host",
        }),
        new webpack.ProvidePlugin({
          React: 'react',
          PropTypes: 'prop-types',
          classNames: 'classnames',
        }),
        new webpack.DefinePlugin({
          __DEV: dev,
        }),
      ])
      .concat(dev ? devPlugins : prodPlugins),

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: dev ? { cacheDirectory: true } : {},
          },
        },
        {
          test: /\.css$/,
          use: [
            dev ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
          ],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: /node_modules/,
          use: [
            dev ? 'style-loader' : MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                modules: {
                  localIdentName: dev
                    ? '[path]__[name]--[local]'
                    : '[hash:base64:5]',
                },
                importLoaders: 2,
                localsConvention: 'camelCase',
              },
            },
            'resolve-url-loader',
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /\.html$/,
          exclude: [/node_modules/, templatesDir],
          use: [
            {
              loader: 'html-loader',
              options: {
                minimize: true,
              },
            },
          ],
        },
        {
          test: /\.(jpg|png|eot|svg|ttf|woff|woff2)?(\?[a-z0-9#=&.]+)?$/,
          use: 'file-loader',
        },
      ],
    },

    devServer: {
      hot: true,
      historyApiFallback: true,
      contentBase: [path.join(__dirname), buildDir],
      publicPath: '/',
    },
  };
}

module.exports = createWebpackConfig();
