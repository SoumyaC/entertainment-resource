const isTest = String(process.env.NODE_ENV) === 'test';

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'usage',
        modules: isTest ? 'commonjs' : false
      }
    ],
    '@babel/preset-react'
  ],
  plugins: [
    // ["@babel/plugin-proposal-decorators", { legacy: true }],
    // ["@babel/plugin-proposal-class-properties", { loose: true }],
    // "@babel/plugin-syntax-dynamic-import", // lets babel parse syntax
    /*
      dynamic-import-node actually transforms the code
      Update 2: exports as a module not a chunk!
      Update 3: works only for babel 6
    */
    // 'dynamic-import-node',

    /*
      that's why we should use dynamic-import instead which sets webpack internal configs to Acorn

      Update 4: somebody made a typo in this library "path.rekatuve" instead of "path.relative"
    */
    // 'dynamic-import',

    /*
      Update 5: again, dynamic-import-node doesn't export as chunk
    */
    // 'dynamic-import-node-babel-7',
    // "@babel/transform-runtime",
    'react-hot-loader/babel'
    // "lodash"
  ]
};
